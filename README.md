MULTI INTENSITY ALGORITHM
VOLTAGE VERSION

Multi Intensity code performs binary search algorithm and sends data using a separate function. The
binary search algorithm is written in the main() function and the send_data() function is used to
transmit the output. The main() function will perform the binary search algorithm.

Here is a brief explanation of the code functionality:

In our code, the binary search algorithm starts by setting the maximum intensity as the initial intensity.
This means that the first step is to check if the voltage reading is below the limit for the maximum
intensity. If it is below the limit, the algorithm sets the maximum intensity as the upper bound and the
middle intensity as the lower bound. If the voltage reading is above the limit, the algorithm sends the
data and sets the next scan position.

After the initial intensity has been set, the algorithm repeatedly checks the voltage reading at the
middle intensity and updates the upper and lower bounds based on the result. If the voltage reading
is below the limit, the upper bound is set to the middle intensity and the middle intensity is updated
to be the average of the upper and lower bounds. If the voltage reading is above the limit, the lower
bound is set to the middle intensity and the middle intensity is updated to be the average of the upper
and lower bounds.

This process continues until the upper and lower bounds are within the specified step size. At this
point, the algorithm sends the data and sets the next scan position. The time complexity of this
algorithm is O(log n), which means that it performs well even with very large lists.

/***************************TEXT BASED FLOW CHART***************************/

Check if the device has reached the end of the scan or the limits are not in range. If it has, then:

Set the latch_up_event flag to 0

Call the send_data() function to transmit the collected data

Set the SysTickScanStep variable to the next scan position using the setNextScanPos() function

Increment SysTickScanStep by 40

Set the PosI variable to the maximum intensity value

Write the PosI value to the MCP4728 device using the MCP4728_MultiWrite() function

If the device has not reached the end of the scan, then check if the new binary middle value has
triggered a latch-up event:

If the voltage is less than the vol_limit and the latch_up_event flag is set, then:

Set the Binary_PosI_max variable to the current Binary_PosImid value

Calculate the new Binary_PosImid value by dividing the difference between Binary_PosI_max and
Binary_PosI_min by 2 and adding the result to Binary_PosI_min

Set the PosI variable to the new Binary_PosImid value

Write the PosI value to the MCP4728 device using the MCP4728_MultiWrite() function

If the voltage is greater than or equal to the vol_limit and the latch_up_event flag is set, then:

Set the Binary_PosI_min variable to the current Binary_PosImid value

Calculate the new Binary_PosImid value by dividing the difference between Binary_PosI_max and
Binary_PosI_min by 2 and adding the result to Binary_PosI_min

Set the PosI variable to the new Binary_PosImid value

Write the PosI value to the MCP4728 device using the MCP4728_MultiWrite() function

Repeat the process till we find the threshold intensity and send data via send_data() function and move
to next scan position.

/******************************* DESCRIPTION******************************/

The condition ((Binary_PosI_max - Binary_PosI_min) <= PosIstep) checks if the device has reached the
end of the scan. If this condition is true, then it sets the latch_up_event to 0, sends the data, sets the
next scan position, increases the scan step by 40, sets the maximum intensity as the current intensity,
and writes the current intensity value to the MCP4728 device.

If the device has not reached the end of the scan, then it checks the following conditions:

If the voltage is less than the limit and the latch-up event has occurred, it updates the maximum
intensity to the current binary middle intensity, calculates the new binary middle intensity, sets the
current intensity to the binary middle intensity, and writes the current intensity value to the MCP
device.

If the voltage is greater than or equal to the limit and the latch-up event has occurred, it updates the
minimum intensity to the current binary middle intensity, calculates the new binary middle intensity,
sets the current intensity to the binary middle intensity, and writes the current intensity value to the
MCP4728 device.

If the voltage is greater than the limit and the latch-up event has not occurred, it sends the data, sets
the next scan position, increases the scan step by 40, sets the maximum intensity as the current
intensity, and writes the current intensity value to the MCP4728 device.

If the voltage is less than the limit and the latch-up event has not occurred, it calculates the new binary
middle intensity, sets the latch-up event to 1, sets the current intensity to the binary middle intensity,
and writes the current intensity value to the MCP4728 device.

The send_data() function sends the reflectivity, intensity, current, and voltage values over UART.


/*******************************************************************************************/

SEL FLAG VERSION
Here is the code explanation for the SEL_FLAG version of the binary search algorithm:

The main() function deals with two arguments: the SEL_FLAG and the intensity.

The code uses the SEL_FLAG variable to detect whether a latch-up event has occurred. The SEL_FLAG
is a binary value that is set in the interrupt service routine for the PORT1_VECTOR interrupt. If a latch-
up event is detected, the SEL_FLAG is set to 1. If no latch-up event is detected, the SEL_FLAG is set to

The main function uses the SEL_FLAG and intensity values to determine whether the device has
reached the end of the scan, and whether a latch-up event has occurred. If the device has reached the
end of the scan, the latch_up_event flag is reset, the send_data function is called with the reflectivity,
current, voltage, and intensity values as arguments, and the scan position is set to the next scan
position.

If a latch-up event has occurred, the main function uses the SEL_FLAG value to determine whether the
latch-up occurred at the current intensity or at a higher intensity. If the latch-up occurred at a higher
intensity, the Binary_PosI_max value is set to the current Binary_PosImid value, and a new
Binary_PosImid value is calculated by dividing the difference between the Binary_PosI_max and
Binary_PosI_min values by 2 and adding the result to the Binary_PosI_min value. Then, the PosI value
is set to the new Binary_PosImid value, and the MCP4728 is updated with the new PosI value.

If the SEL_FLAG is less than 1 and the latch-up event has not occurred (latch_up_event == 0), then the
send_data() function is called to transmit the reflectivity, current, voltage, and intensity values over
UART. The next scan position is set using the setNextScanPos() function and the intensity value is
updated to the maximum intensity value. The new intensity value is then written to the MCP4728 DAC.

If a latch-up event is detected, the code adjusts the binary search limits based on the value of
SEL_FLAG. If SEL_FLAG is greater than 0, the maximum intensity limit is updated to the current binary
search middle point. If SEL_FLAG is less than 1, the minimum intensity limit is updated to the current
binary search middle point.

If no latch-up event is detected, the code sends the data and proceeds to the next scan position if
SEL_FLAG is less than 1. If SEL_FLAG is greater than 0, the code calculates the next binary search middle
point and continues the search for the threshold intensity.

Overall, this code uses SEL_FLAG to detect latch-up events and adjust the binary search accordingly,
to find the threshold intensity for the device.
